
import './App.css';
import React from 'react';
import {Redirect, Route} from "react-router-dom";
import { BrowserRouter as Router } from 'react-router-dom';
import Search from './spotifyCtrl';

function App() {

  
  return (
    <Router>
      <Route path="/" exact>
      <Redirect to="/searchsong" />
      </Route>
      <Route path="/searchsong">
        <Search></Search>
      </Route>
    </Router>
  );
}

export default App;
