import React,{useState} from 'react';
import Card from './card';
import styled from "styled-components";


const Search = (props) => {

    const [searchValue,setSearchValue]=useState("");
    const [filterBySongName,setFilterBySongName]=useState("");
    const [results,setResults]=useState(null);
    const[filteredResults,setFilteredResults]=useState([]);
    const searchSongs = async() => {
        const response = await fetch(`http://localhost:5000/searchartist/${searchValue}&limit=20`, {
            method: 'GET',
           

        })
        .then(response=>{
           // debugger;
             return response.json()   ;
           
        }).then(data=>{
            debugger;
            setResults(data.message.tracks.items);
            setFilteredResults(data.message.tracks.items);
        })
    }
    const setValue=(e)=>{
        setSearchValue(e.target.value);
    }
    const filterResults=(e)=>{  
        if(e.target.value != "")   
        {
            setFilterBySongName(e.target.value);
            var res= results.filter((item)=>{
                 return item.name.toLowerCase().indexOf(e.target.value.toLowerCase())>=0;
             })
            return setFilteredResults(res);
        }
        else
        {
            setFilterBySongName(e.target.value);
            setFilteredResults(results);
            return results;
        }
        
     
      
  
    }

    return (
        <StyledMain>
         
              <StyledDiv> Search Artist   </StyledDiv>
              <StyledInput placeholder="Search a Track" value={searchValue} onChange={setValue}/>
              <StyledInput  placeholder="filter by Song name" value={filterBySongName} onChange={filterResults}/>  
             
                <div><button style={{ textAlign: 'center', width: '100px', height: '30px', marginTop: '30px', borderRadius: '5px' }} onClick={searchSongs}>search song</button></div>
        

            { filteredResults.length>0 &&
                <Card searchResults={filteredResults}></Card>
            }
             
             </StyledMain>
     

    )

  
}
const StyledMain = styled.main`
    text-align:center;

`;
const StyledInput = styled.input`
height:30px;
width:200px !important;
text-align: center;
margin:10px !important;
height:30px;
text-align:center;
`;
const StyledDiv = styled.div`
    
    width:50%;
    margin-top:10px !important;
    margin-buttom:10px !important;  
 
    margin: auto; 
    text-align:center;
`;




export default Search;




  // const searchSongsClient = async() => {
    //     const response = await fetch("https://api.spotify.com/v1/search?q=sting&type=track", {
    //         method: 'GET',
    //         mode:"no-cors",
    //         headers: {
    //             'Access-Control-Allow-Origin': '*',
    //             'Content-Type': 'application/json',
    //             'Authorization':'Bearer BQDXLyIRi5s4hUu88hmesSIC7uGb4dxqWbE4e7HFhuTfbLY5Gxs_YzQ9rtmpMlko36_Svv8v-zth2qRfmI1pZnrZyvj9O62Vho5NCNBu8YAwVohxLbhrhSxViaYIQRkNXCMIxoxrr-UhFl3saGoZZCFoHGght3OW0pSFCgT3'

    //             // 'Content-Type': 'application/x-www-form-urlencoded',
    //           },
              
          

    //     })
    //     .then(response=>{
           
    //          return response.json()   ;
           
    //     }).then(data=>{
    //         setResults(data.message);
    //     })
     
   
    //     //const responseData = response.json();
        
    //    // console.log(responseData);
    // }